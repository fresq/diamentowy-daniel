/*
 * pid.h
 *
 *  Created on: 25.06.2017
 *      Author: Tomek
 */

#ifndef PID_H_
#define PID_H_

#include "IMU.h"
struct pidObject{
	int16_t Kp;
	int16_t Ki;
	int16_t Kd;
	int16_t output;
	float priorError;
	int16_t derivative;
	int16_t integral;
	float error;
	int16_t target;
};
struct pidObject balans;
struct pidObject lf;

void linefollowerInit() {
	lf.Kp = 100;
	lf.Ki = 0;
	lf.Kd = 500;
	lf.output = 0;
	lf.priorError = 0;
	lf.derivative = 0;
	lf.integral = 0;
	lf.error = 0;
	lf.target = 150;
}

void balansInit() {
	balans.Kp = 90;
	balans.Ki = 0;
	balans.Kd = 0;
	balans.output = 0;
	balans.priorError = 0;
	balans.derivative = 0;
	balans.integral = 0;
	balans.error = 0;
	balans.target = 83;
}
uint16_t adc[5];
uint8_t ktir_treshold[4];
int8_t wagi[4] = { -4, -2, 2, 4 };
uint16_t treshold = 1500;
//volatile uint16_t Kp = 100, Kd = 500;
//int16_t regulacja = 0, regulacjaPD, predkosc = 150;
int16_t roznica, poprzedniBlad = 0, bladNaLinii = 0, blad = 0, ostatniBlad = 0;

int16_t predkoscLewy, predkoscPrawy;
/************************ PID *************************/
void balancing() {
	balans.error = (roll - balans.target);
	balans.derivative = balans.error - balans.priorError;
	balans.integral += balans.error * dt;
	balans.output = -(balans.Kp * balans.error + balans.Kd * balans.derivative
			+ balans.Ki * balans.integral);
	balans.priorError = balans.error;
	drive(balans.output, balans.output);
}

/************************ PD *************************/
int16_t calculateError() {
	int8_t i, ile = 0;
	int16_t sum = 0;

	for (i = 0; i < 4; ++i) {
		if (adc[i] > treshold) {
			ktir_treshold[i] = 1;
			++ile;
			sum += wagi[i];
		} else
			ktir_treshold[i] = 0;
	}
	if (adc[0] > treshold || adc[1] > treshold || adc[2] > treshold || adc[3] > treshold) //zapamietanie ostatniego bledu podczas jazdy po linii
		bladNaLinii = poprzedniBlad;
	if (ile > 0) {		// je�li kt�ry� czujnik wykrywa lini�
		blad = sum / ile;
		poprzedniBlad = blad;
		return blad;
	} else {         // jesli �aden czujnik nie wykrywa linii

		if (adc[0] > treshold && adc[1] > treshold && adc[2] > treshold && adc[3] > treshold // jesli wyjedzie na raz wszystkimi czujnikami i ma blad 0
		&& blad == 0) {
			if (bladNaLinii > 0)
				blad = 4;
			if (bladNaLinii < 0)
				blad = -4;
		}

		if (poprzedniBlad < -3) // linia ostatanio widziana po lewej stronie - ustalamy ujemny b��d wi�kszy od b��du skrajnego lewego czujnika
			blad = -4;

		else if (poprzedniBlad > 3) // linia ostatanio widziana po prawej stronie - analogicznie do powy�szego
			blad = 4;
		return blad; // <-- tu powinno byc chyba return blad;
	}
}

void linefollower() {
	lf.error = calculateError();
	lf.derivative = lf.error - lf.priorError;
	lf.integral += lf.error * dt;
	lf.output = lf.Kp * lf.error + lf.Kd * lf.derivative
			+ lf.Ki * lf.integral;
	lf.priorError = lf.error;
	drive(lf.target - lf.output, lf.target + lf.output);
}
#endif /* PID_H_ */
