/*
 * communication.h
 *
 *  Created on: 25.06.2017
 *      Author: Tomek
 */

#ifndef COMMUNICATION_H_
#define COMMUNICATION_H_
#include "usart.h"
#include "IMU.h"
#include "motors.h"
#include "pid.h"
#include <limits.h>
#include <math.h>
uint8_t wybor = 0, rosnij = 0, przycisk = 0, flag = 0;
uint16_t brightness = 0;
//BT//
int8_t BTData[20];
uint8_t BTReceive[20];

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) { // obsluga BT przez dma z przerwaniami
	switch (BTReceive[0]) {
	case 0x01:
		drive(0, 0);
		break;
	case 0x00:
		drive(100, 100);
		break;
	}
	if (BTReceive[0])
		drive(100, 100);
	HAL_UART_Receive_DMA(&huart1, BTReceive, 1);
}

void SendBtData() {
	BTData[0] = (lf.Kp >> 8) & 0x00FF;
	BTData[1] = lf.Kp & 0x00FF;
	BTData[2] = (lf.Kd >> 8) & 0x00FF;
	BTData[3] = lf.Kd & 0x00FF;
	BTData[4] = ((uint16_t)lf.error >> 8) & 0x00FF;
	BTData[5] = (uint16_t)lf.error & 0x00FF;
	BTData[6] = (lf.output >> 8) & 0x00FF;
	BTData[7] = lf.output & 0x00FF;
	BTData[10] = (lf.target >> 8) & 0x00FF;
	BTData[11] = lf.target & 0x00FF;
	BTData[12] = (estroll >> 8) & 0x00FF;
	BTData[13] = estroll & 0x00FF;
	BTData[14] = (estpitch >> 8) & 0x00FF;
	BTData[15] = estpitch & 0x00FF;
	BTData[16] = (Xacc >> 8) & 0x00FF;
	BTData[17] = Xacc & 0x00FF;
	BTData[18] = (Yacc >> 8) & 0x00FF;
	BTData[19] = Yacc & 0x00FF;
	BTData[8] = (Zacc >> 8) & 0x00FF;
	BTData[9] = Zacc & 0x00FF;
	HAL_UART_Transmit_DMA(&huart1, BTData, 20);
}

void leftDiode(int16_t brightness) {
	TIM16->CCR1 = brightness;
}
void rightDiode(int16_t brightness) {
	TIM2->CCR1 = brightness;
}
void diodesPulsation() {
	if (brightness <= 0)
		rosnij = 1;
	if (brightness >= 400)
		rosnij = 0;
	if (rosnij) {
		if (brightness < 200)
			brightness = brightness + 2;
		else
			brightness = brightness + 4;
	} else {
		if (brightness < 200)
			brightness = brightness - 2;
		else
			brightness = brightness - 4;
	}
	TIM16->CCR1 = brightness;
	TIM2->CCR1 = brightness;
}
#endif /* COMMUNICATION_H_ */
