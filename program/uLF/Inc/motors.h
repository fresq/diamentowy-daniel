/*
 * motors.h
 *
 *  Created on: 25.06.2017
 *      Author: Tomek
 */

#ifndef MOTORS_H_
#define MOTORS_H_

const int16_t maxPWM = 1000;

void drive(int16_t left, int16_t right) {
	if (left > maxPWM) {
		left = maxPWM;
	} else if (left < -maxPWM) {
		left = -maxPWM;
	}
	if (right > maxPWM) {
		right = maxPWM;
	} else if (right < -maxPWM) {
		right = -maxPWM;
	}
	if (right >= 0) {
		TIM3->CCR2 = right;
		HAL_GPIO_WritePin(AIN1_GPIO_Port, AIN1_Pin, 0);	//Prawe do przodu
		HAL_GPIO_WritePin(AIN2_GPIO_Port, AIN2_Pin, 1);
	} else if (right < 0) {
		TIM3->CCR2 = -right;
		HAL_GPIO_WritePin(AIN1_GPIO_Port, AIN1_Pin, 1);	//Prawe do tylu
		HAL_GPIO_WritePin(AIN2_GPIO_Port, AIN2_Pin, 0);
	}

	if (left >= 0) {
		TIM3->CCR1 = left;
		HAL_GPIO_WritePin(BIN1_GPIO_Port, BIN1_Pin, 0);	//Lewe do przodu
		HAL_GPIO_WritePin(BIN2_GPIO_Port, BIN2_Pin, 1);
	} else if (left < 0) {
		TIM3->CCR1 = -left;
		HAL_GPIO_WritePin(BIN1_GPIO_Port, BIN1_Pin, 1);	//Lewe do tylu
		HAL_GPIO_WritePin(BIN2_GPIO_Port, BIN2_Pin, 0);
	}
}
#endif /* MOTORS_H_ */
