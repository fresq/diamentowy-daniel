/*
 * IMU.h
 *
 *  Created on: 8 mar 2017
 *      Author: Tomek Kozlowski
 */

#ifndef IMU_H_
#define IMU_H_

#include <limits.h>
#include <stdlib.h>
#include <math.h>
#include "stm32f0xx_hal.h"
#include "i2c.h"
//#define M_PI 3.14159265359
//////IMU///////
//rejestry
#define ACC_GYRO_ADDRESS (0x6B << 1) // adres akcelerometru:  1101010x
#define IMU_CTRL_REG1_G 0x10 // rejestr ustawien g 1 - czestotliwosc, rozdzielczosc
#define IMU_CTRL_REG2_G 0x11 // rejestr ustawien g 2 - przerwania, konfiguracja wyjscia
#define IMU_CTRL_REG3_G 0x12 // rejestr ustawien g 3 - filtr gornoprzepustowy, tryb niskiego poboru mocy
#define IMU_CTRL_REG6_XL 0x20 // rejestr ustawien acc 6
#define IMU_CTRL_REG7_XL 0x21 // rejestr ustawien acc 7
#define IMU_CTRL_REG8 0x22 // rejestr ustawien 8
#define ACC_X_L 0x28 // nizszy bajt danych osi x
#define GYRO_X_L 0x18 // nizszy bajt danych osi x

#define GYRO_SETTINGS2 0x2 //0000 0010
#define GYRO_SETTINGS3 0x49 //0100 1001
//CTRL_REG1_G = [ODR_G2][ODR_G1][ODR_G0][FS_G1][FS_G0][0][BW_G1][BW_G0]
#define GYRO_SETTINGS1 0xC8  //1100 1000 500 dps, 952 Hz
// CTRL_REG6_XL = [ODR_XL2][ODR_XL1][ODR_XL0][FS1_XL][FS0_XL][BW_SCAL_ODR][BW_XL1][BW_XL0]
#define ACC_SETTINGS6 0xB0 // 1011 0000 4g, ODR = 476 Hz
//#define ACC_SETTINGS6 0xC0 // 1100 0000 2g, ODR = 952 Hz
#define ACC_SETTINGS7 0xC1	//1110 0101 filtry acc - ODR/9
//#define ACC_SETTINGS7 0x65	//0110 0101 filtry acc - ODR/9, bez high resolution mode
// CTRL_REG8 = [BOOT][BDU][H_LACTIVE][PP_OD][SIM][ID_ADD_INC][BLE][SW_RESET]
#define IMU_SETTINGS8 0x4 // 0000 0100 auto increment
#define SW_RESET 0x5 // 0000 0101 auto increment
#define ACC_RESOLUTION 4	//g
#define GYRO_RESOLUTION 500	//dps
////////////////

uint8_t ImuData[6] = { 0 }; // Zmienna do bezposredniego odczytu z akcelerometru
int16_t Xacc = 0, Yacc = 0, Zacc = 0;
float Xacc_mps = 0, Yacc_mps = 0, Zacc_mps = 0; // Zawiera przyspieszenie liniowe przekstalcone na jednostke fizyczna [g]
int16_t Xgyro = 0, Ygyro = 0, Zgyro = 0;
float Xgyro_dps = 0, Ygyro_dps = 0, Zgyro_dps = 0; // Zawiera predkosc katowa przekstalcona na jednostke fizyczna [dps]
uint8_t Settings;
const float dt = 0.005;  // s
float g = 9.8113;
float Xacc_filtered, Yacc_filtered, Zacc_filtered;
float lastAcceleration = 0, lastVelocity = 0, lastPosition = 0;
float lastXgyro_dps = 0, lastYgyro_dps = 0, lastZgyro_dps=0;
float acceleration = 0, velocity = 0, position = 0;
int16_t estroll, estpitch;
float pitch = 0, roll = 85, yaw = 0;
///prototypy///
void CzytajIMU();
void imuConfig();
void KalmFilterInit();
void Kalman();
void LiczPredkosc();
void ComplementaryFilter();
void RestartImu();
///////////////



void KalmFilterInit() {
	kalm.q = 0.00001;
	kalm.r = 0.0005;

	kalm.measured_data1 = 0;
	kalm.Pk11 = kalm.q;
	kalm.Pkp1 = 0;
	kalm.K1 = 0;
	kalm.Xk1 = 0;

	kalm.measured_data2 = 0;
	kalm.Pk12 = kalm.q;
	kalm.Pkp2 = 0;
	kalm.K2 = 0;
	kalm.Xk2 = 0;

	kalm.measured_data3 = 0;
	kalm.Pk13 = kalm.q;
	kalm.Pkp3 = 0;
	kalm.K3 = 0;
	kalm.Xk3 = 0;
}

void ComplementaryFilter() {
	float pitchAcc, rollAcc;
	// Integrate the gyroscope data -> int(angularSpeed) = angle
	pitch +=Xgyro_dps * dt; // Angle around the X-axis
	roll -= Ygyro_dps * dt;    // Angle around the Y-axis
	yaw += Zgyro_dps * dt;		  // Angle around the Z-axis
	// Compensate for drift with accelerometer data if !bullshit
	// Sensitivity = -2 to 2 G at 16Bit -> 2G = 32768 && 0.5G = 8192
	int forceMagnitudeApprox = abs(Xacc_mps) + abs(Yacc_mps) + abs(Zacc_mps);
	if (forceMagnitudeApprox > 5 && forceMagnitudeApprox < 20) {
		// Turning around the X axis results in a vector on the Y-axis
		pitchAcc = atan2f(Yacc_mps, Zacc_mps) * 180 / M_PI;
		pitch = pitch * 0.98 + pitchAcc * 0.02;
		estpitch = (int16_t)pitch;
		// Turning around the Y axis results in a vector on the X-axis
		rollAcc = atan2f(Xacc_mps, Zacc_mps) * 180 / M_PI;
		roll = roll * 0.8 + rollAcc * 0.2;
		estroll = (int16_t) roll;
	}
}
void Kalman() {
	kalm.measured_data1 = Xacc_mps;
	kalm.Pkp1 = kalm.Pk11 + kalm.q;
	kalm.K1 = (kalm.Pkp1) / (kalm.Pkp1 + kalm.r);
	kalm.Xk1 = kalm.Xk1 + kalm.K1 * (kalm.measured_data1 - kalm.Xk1);
	kalm.Pk11 = (1 - kalm.K1) * kalm.Pkp1;
	Xacc_filtered = kalm.Xk1;

	kalm.measured_data2 = Yacc_mps;
	kalm.Pkp2 = kalm.Pk12 + kalm.q;
	kalm.K2 = (kalm.Pkp2) / (kalm.Pkp2 + kalm.r);
	kalm.Xk2 = kalm.Xk2 + kalm.K2 * (kalm.measured_data2 - kalm.Xk2);
	kalm.Pk12 = (1 - kalm.K2) * kalm.Pkp2;
	Yacc_filtered = kalm.Xk2;

	kalm.measured_data3 = Zacc_mps;
	kalm.Pkp3 = kalm.Pk13 + kalm.q;
	kalm.K3 = (kalm.Pkp3) / (kalm.Pkp3 + kalm.r);
	kalm.Xk3 = kalm.Xk3 + kalm.K3 * (kalm.measured_data3 - kalm.Xk3);
	kalm.Pk13 = (1 - kalm.K3) * kalm.Pkp3;
	Zacc_filtered = kalm.Xk3;
}

void LiczPredkosc() {
	acceleration = (sqrt(
			(Xacc_filtered) * (Xacc_filtered)
					+ (Yacc_filtered) * (Yacc_filtered)
					+ (Zacc_filtered) * (Zacc_filtered))) * 100;
	velocity += ((lastAcceleration + acceleration) / 2) * dt;
//	velocity+=(sqrt(
//			(Xacc_mps) * (Xacc_mps)
//					+ (Yacc_mps) * (Yacc_mps)
//					+ (Zacc_mps) * (Zacc_mps)))*dt;
//	position += ((lastVelocity+velocity)/2)*dt;
	//velocity += acceleration*dt;
	lastAcceleration = acceleration;
	//lastVelocity = velocity;
	//lastPosition = position;
}

void CzytajIMU() {
	HAL_I2C_Mem_Read(&hi2c1, ACC_GYRO_ADDRESS, (ACC_X_L), 1, ImuData, 6, 100);
	Xacc = ((ImuData[1] << 8) | ImuData[0]);
	Yacc = ((ImuData[3] << 8) | ImuData[2]);
	Zacc = ((ImuData[5] << 8) | ImuData[4]);
	Xacc_mps = (((float) Xacc * ACC_RESOLUTION * g) / (float) INT16_MAX)
			+ 0.3863f;
	Yacc_mps = (((float) Yacc * ACC_RESOLUTION * g) / (float) INT16_MAX)
			+ 0.02f;
	Zacc_mps = (((float) Zacc * ACC_RESOLUTION * g) / (float) INT16_MAX)
			+ 0.5852f;

	HAL_I2C_Mem_Read(&hi2c1, ACC_GYRO_ADDRESS, (GYRO_X_L), 1, ImuData, 6, 100);
	Xgyro = ((ImuData[1] << 8) | ImuData[0]);
	Ygyro = ((ImuData[3] << 8) | ImuData[2]);
	Zgyro = ((ImuData[5] << 8) | ImuData[4]);
	lastXgyro_dps = Xgyro_dps;
	lastYgyro_dps = Ygyro_dps;
	lastZgyro_dps = Zgyro_dps;
	Xgyro_dps = ((float) Xgyro * GYRO_RESOLUTION) / (float) INT16_MAX;
	Ygyro_dps = ((float) Ygyro * GYRO_RESOLUTION) / (float) INT16_MAX;
	Zgyro_dps = ((float) Zgyro * GYRO_RESOLUTION) / (float) INT16_MAX;
}
void imuConfig() {
	// uruchomienie autoinkrementacji adresu
	Settings = IMU_SETTINGS8;
	HAL_I2C_Mem_Write(&hi2c1, ACC_GYRO_ADDRESS, IMU_CTRL_REG8, 1, &Settings, 1,
			100);
	// uruchomienie akcelerometru
	Settings = ACC_SETTINGS6;
	HAL_I2C_Mem_Write(&hi2c1, ACC_GYRO_ADDRESS, IMU_CTRL_REG6_XL, 1, &Settings,
			1, 100);
	// filtry acc
	Settings = ACC_SETTINGS7;
	HAL_I2C_Mem_Write(&hi2c1, ACC_GYRO_ADDRESS, IMU_CTRL_REG7_XL, 1, &Settings,
			1, 100);
	// uruchomienie zyroskopu
	Settings = GYRO_SETTINGS1;
	HAL_I2C_Mem_Write(&hi2c1, ACC_GYRO_ADDRESS, IMU_CTRL_REG1_G, 1, &Settings,
			1, 100);
	Settings = GYRO_SETTINGS2;
	HAL_I2C_Mem_Write(&hi2c1, ACC_GYRO_ADDRESS, IMU_CTRL_REG2_G, 1, &Settings,
			1, 100);
	Settings = GYRO_SETTINGS3;
	HAL_I2C_Mem_Write(&hi2c1, ACC_GYRO_ADDRESS, IMU_CTRL_REG3_G, 1, &Settings,
			1, 100);

}
void RestartImu(){
	Settings = SW_RESET;
		HAL_I2C_Mem_Write(&hi2c1, ACC_GYRO_ADDRESS, IMU_CTRL_REG8, 1, &Settings, 1,
				100);
		imuConfig();
}

#endif /* IMU_H_ */
