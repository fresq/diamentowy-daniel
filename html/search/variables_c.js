var searchData=
[
  ['m',['M',['../structarm__fir__decimate__instance__q15.html#aad9320284218b3aa378527ea518cf093',1,'arm_fir_decimate_instance_q15::M()'],['../structarm__fir__decimate__instance__q31.html#ad3d6936c36303b30dd38f1eddf248ae5',1,'arm_fir_decimate_instance_q31::M()'],['../structarm__fir__decimate__instance__f32.html#a76a8b2161731638eb3d67f277919f95d',1,'arm_fir_decimate_instance_f32::M()']]],
  ['mask0',['MASK0',['../struct_d_w_t___type.html#a821eb5e71f340ec077efc064cfc567db',1,'DWT_Type']]],
  ['mask1',['MASK1',['../struct_d_w_t___type.html#aabf94936c9340e62fed836dcfb152405',1,'DWT_Type']]],
  ['mask2',['MASK2',['../struct_d_w_t___type.html#a00ac4d830dfe0070a656cda9baed170f',1,'DWT_Type']]],
  ['mask3',['MASK3',['../struct_d_w_t___type.html#a2a509d8505c37a3b64f6b24993df5f3f',1,'DWT_Type']]],
  ['maxdelay',['maxDelay',['../structarm__fir__sparse__instance__f32.html#af8b8c775f4084c36774f06c082b4c078',1,'arm_fir_sparse_instance_f32::maxDelay()'],['../structarm__fir__sparse__instance__q31.html#afdd3a1dc72132c854dc379154b68b674',1,'arm_fir_sparse_instance_q31::maxDelay()'],['../structarm__fir__sparse__instance__q15.html#ad14cc1070eecf7e1926d8f67a8273182',1,'arm_fir_sparse_instance_q15::maxDelay()'],['../structarm__fir__sparse__instance__q7.html#af74dacc1d34c078283e50f2530eb91df',1,'arm_fir_sparse_instance_q7::maxDelay()']]],
  ['mmfar',['MMFAR',['../struct_s_c_b___type.html#a2d03d0b7cec2254f39eb1c46c7445e80',1,'SCB_Type']]],
  ['mmfr',['MMFR',['../struct_s_c_b___type.html#a4f353f207bb27a1cea7861aa9eb00dbb',1,'SCB_Type']]],
  ['mu',['mu',['../structarm__lms__instance__f32.html#ae2af43d74c93dba16b876e10c97a5b99',1,'arm_lms_instance_f32::mu()'],['../structarm__lms__instance__q15.html#aae46129d7cfd7f1c162cc502ed0a9d49',1,'arm_lms_instance_q15::mu()'],['../structarm__lms__instance__q31.html#acb6ca9996b3c5f740d5d6c8e9f4f1d46',1,'arm_lms_instance_q31::mu()'],['../structarm__lms__norm__instance__f32.html#a84401d3cfc6c40f69c08223cf341b886',1,'arm_lms_norm_instance_f32::mu()'],['../structarm__lms__norm__instance__q31.html#ad3dd2a2406e02fdaa7782ba6c3940a64',1,'arm_lms_norm_instance_q31::mu()'],['../structarm__lms__norm__instance__q15.html#a7ce00f21d11cfda6d963240641deea8c',1,'arm_lms_norm_instance_q15::mu()']]],
  ['mvfr0',['MVFR0',['../struct_s_c_b___type.html#a7a1ba0f875c0e97c1673882b1106e66b',1,'SCB_Type']]],
  ['mvfr1',['MVFR1',['../struct_s_c_b___type.html#a75d6299150fdcbbcb765e22ff27c432e',1,'SCB_Type']]],
  ['mvfr2',['MVFR2',['../struct_s_c_b___type.html#a280ef961518ecee3ed43a86404853c3d',1,'SCB_Type']]]
];
