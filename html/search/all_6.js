var searchData=
[
  ['fpu_20functions',['FPU Functions',['../group___c_m_s_i_s___core___fpu_functions.html',1,'']]],
  ['functions_20and_20instructions_20reference',['Functions and Instructions Reference',['../group___c_m_s_i_s___core___function_interface.html',1,'']]],
  ['ffcr',['FFCR',['../struct_t_p_i___type.html#a3f68b6e73561b4849ebf953a894df8d2',1,'TPI_Type']]],
  ['ffsr',['FFSR',['../struct_t_p_i___type.html#a6c47a0b4c7ffc66093ef993d36bb441c',1,'TPI_Type']]],
  ['fftlen',['fftLen',['../structarm__cfft__radix2__instance__q15.html#a874085647351dcf3f0de39d2b1d49744',1,'arm_cfft_radix2_instance_q15::fftLen()'],['../structarm__cfft__radix4__instance__q15.html#a5fc543e7d84ca8cb7cf6648970f21ca6',1,'arm_cfft_radix4_instance_q15::fftLen()'],['../structarm__cfft__radix2__instance__q31.html#a960199f1373a192366878ef279eab00f',1,'arm_cfft_radix2_instance_q31::fftLen()'],['../structarm__cfft__radix4__instance__q31.html#ab413d2a5d3f45fa187d93813bf3bf81b',1,'arm_cfft_radix4_instance_q31::fftLen()'],['../structarm__cfft__radix2__instance__f32.html#a2f915a1c29635c1623086aaaa726be8f',1,'arm_cfft_radix2_instance_f32::fftLen()'],['../structarm__cfft__radix4__instance__f32.html#a7e6a6d290ce158ce9a15a45e364b021a',1,'arm_cfft_radix4_instance_f32::fftLen()'],['../structarm__cfft__instance__q15.html#a5f9e1d3a8c127ee323b5e6929aeb90df',1,'arm_cfft_instance_q15::fftLen()'],['../structarm__cfft__instance__q31.html#a4406f23e8fd0bff8d555225612e2a2a8',1,'arm_cfft_instance_q31::fftLen()'],['../structarm__cfft__instance__f32.html#acd8f9e9540e3dd348212726e5d6aaa95',1,'arm_cfft_instance_f32::fftLen()']]],
  ['fftlenby2',['fftLenBy2',['../structarm__rfft__instance__f32.html#a075076e07ebb8521d8e3b49a31db6c57',1,'arm_rfft_instance_f32']]],
  ['fftlenreal',['fftLenReal',['../structarm__rfft__instance__q15.html#aac5cf9e825917cbb14f439e56bb86ab3',1,'arm_rfft_instance_q15::fftLenReal()'],['../structarm__rfft__instance__q31.html#af777b0cadd5abaf064323692c2e6693b',1,'arm_rfft_instance_q31::fftLenReal()'],['../structarm__rfft__instance__f32.html#a4219d4669699e4efdcb150ed7a0d9a57',1,'arm_rfft_instance_f32::fftLenReal()']]],
  ['fftlenrfft',['fftLenRFFT',['../structarm__rfft__fast__instance__f32.html#aef06ab665041ec36f5b25d464f0cab14',1,'arm_rfft_fast_instance_f32']]],
  ['fifo0',['FIFO0',['../struct_t_p_i___type.html#aa4d7b5cf39dff9f53bf7f69bc287a814',1,'TPI_Type']]],
  ['fifo1',['FIFO1',['../struct_t_p_i___type.html#a061372fcd72f1eea871e2d9c1be849bc',1,'TPI_Type']]],
  ['foldcnt',['FOLDCNT',['../struct_d_w_t___type.html#a1cfc48384ebd8fd8fb7e5d955aae6c97',1,'DWT_Type']]],
  ['fpca',['FPCA',['../union_c_o_n_t_r_o_l___type.html#ac62cfff08e6f055e0101785bad7094cd',1,'CONTROL_Type']]],
  ['fscr',['FSCR',['../struct_t_p_i___type.html#ad6901bfd8a0089ca7e8a20475cf494a8',1,'TPI_Type']]],
  ['function0',['FUNCTION0',['../struct_d_w_t___type.html#a579ae082f58a0317b7ef029b20f52889',1,'DWT_Type']]],
  ['function1',['FUNCTION1',['../struct_d_w_t___type.html#a8dfcf25675f9606aa305c46e85182e4e',1,'DWT_Type']]],
  ['function2',['FUNCTION2',['../struct_d_w_t___type.html#ab1b60d6600c38abae515bab8e86a188f',1,'DWT_Type']]],
  ['function3',['FUNCTION3',['../struct_d_w_t___type.html#a52d4ff278fae6f9216c63b74ce328841',1,'DWT_Type']]],
  ['fast_20math_20functions',['Fast Math Functions',['../group__group_fast_math.html',1,'']]],
  ['filtering_20functions',['Filtering Functions',['../group__group_filters.html',1,'']]]
];
