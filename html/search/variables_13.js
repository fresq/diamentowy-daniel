var searchData=
[
  ['t',['T',['../unionx_p_s_r___type.html#a7eed9fe24ae8d354cd76ae1c1110a658',1,'xPSR_Type']]],
  ['tcr',['TCR',['../struct_i_t_m___type.html#a04b9fbc83759cb818dfa161d39628426',1,'ITM_Type']]],
  ['ter',['TER',['../struct_i_t_m___type.html#acd03c6858f7b678dab6a6121462e7807',1,'ITM_Type']]],
  ['tpr',['TPR',['../struct_i_t_m___type.html#ae907229ba50538bf370fbdfd54c099a2',1,'ITM_Type']]],
  ['trigger',['TRIGGER',['../struct_t_p_i___type.html#a4d4cd2357f72333a82a1313228287bbd',1,'TPI_Type']]],
  ['twidcoefmodifier',['twidCoefModifier',['../structarm__cfft__radix2__instance__q15.html#a6f2ab87fb4c568656e1f92f687b5c850',1,'arm_cfft_radix2_instance_q15::twidCoefModifier()'],['../structarm__cfft__radix4__instance__q15.html#af32fdc78bcc27ca385f9b76a0a1f71c3',1,'arm_cfft_radix4_instance_q15::twidCoefModifier()'],['../structarm__cfft__radix2__instance__q31.html#ae63ca9193322cd477970c1d2086407d1',1,'arm_cfft_radix2_instance_q31::twidCoefModifier()'],['../structarm__cfft__radix4__instance__q31.html#a8cf8187b8232815cf17ee82bf572ecf9',1,'arm_cfft_radix4_instance_q31::twidCoefModifier()'],['../structarm__cfft__radix2__instance__f32.html#a411f75b6ed01690293f4f5988030ea42',1,'arm_cfft_radix2_instance_f32::twidCoefModifier()'],['../structarm__cfft__radix4__instance__f32.html#abe31ea2157dfa233e389cdfd3b9993ee',1,'arm_cfft_radix4_instance_f32::twidCoefModifier()']]],
  ['twidcoefrmodifier',['twidCoefRModifier',['../structarm__rfft__instance__q15.html#afd444d05858c5f419980e94e8240d5c3',1,'arm_rfft_instance_q15::twidCoefRModifier()'],['../structarm__rfft__instance__q31.html#a6fc90252b579f7c29e01bd279334fc43',1,'arm_rfft_instance_q31::twidCoefRModifier()'],['../structarm__rfft__instance__f32.html#aede85350fb5ae6baa1b3e8bfa15b18d6',1,'arm_rfft_instance_f32::twidCoefRModifier()']]]
];
