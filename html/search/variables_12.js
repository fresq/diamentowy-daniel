var searchData=
[
  ['scr',['SCR',['../struct_s_c_b___type.html#a3a4840c6fa4d1ee75544f4032c88ec34',1,'SCB_Type']]],
  ['sfcr',['SFCR',['../struct_s_c_b___type.html#a82273352d2e8c7a28a7b7cbdfc3d6a75',1,'SCB_Type']]],
  ['shcsr',['SHCSR',['../struct_s_c_b___type.html#a7b5ae9741a99808043394c4743b635c4',1,'SCB_Type']]],
  ['shp',['SHP',['../struct_s_c_b___type.html#a5c40d124f95a3f7f431a3d5409d6ad28',1,'SCB_Type::SHP()'],['../struct_s_c_b___type.html#a9b05f74580fc93daa7fe2f0e1c9c5663',1,'SCB_Type::SHP()']]],
  ['shpr',['SHPR',['../struct_s_c_b___type.html#afdab23abd301033bb318c7b188b377db',1,'SCB_Type']]],
  ['sint',['Sint',['../structarm__rfft__fast__instance__f32.html#a37419ababdfb3151b1891ae6bcd21012',1,'arm_rfft_fast_instance_f32']]],
  ['sleepcnt',['SLEEPCNT',['../struct_d_w_t___type.html#a416a54e2084ce66e5ca74f152a5ecc70',1,'DWT_Type']]],
  ['sppr',['SPPR',['../struct_t_p_i___type.html#a12f79d4e3ddc69893ba8bff890d04cc5',1,'TPI_Type']]],
  ['spsel',['SPSEL',['../union_c_o_n_t_r_o_l___type.html#a8cc085fea1c50a8bd9adea63931ee8e2',1,'CONTROL_Type']]],
  ['sspsr',['SSPSR',['../struct_t_p_i___type.html#a7b72598e20066133e505bb781690dc22',1,'TPI_Type']]],
  ['state',['state',['../structarm__pid__instance__q15.html#a4a3f0a878b5b6b055e3478a2f244cd30',1,'arm_pid_instance_q15::state()'],['../structarm__pid__instance__q31.html#a228e4a64da6014844a0a671a1fa391d4',1,'arm_pid_instance_q31::state()'],['../structarm__pid__instance__f32.html#afd394e1e52fb1d526aa472c83b8f2464',1,'arm_pid_instance_f32::state()']]],
  ['stateindex',['stateIndex',['../structarm__fir__sparse__instance__f32.html#a57585aeca9dc8686e08df2865375a86d',1,'arm_fir_sparse_instance_f32::stateIndex()'],['../structarm__fir__sparse__instance__q31.html#a557ed9d477e76e4ad2019344f19f568a',1,'arm_fir_sparse_instance_q31::stateIndex()'],['../structarm__fir__sparse__instance__q15.html#a89487f28cab52637426024005e478985',1,'arm_fir_sparse_instance_q15::stateIndex()'],['../structarm__fir__sparse__instance__q7.html#a2d2e65473fe3a3f2b953b4e0b60824df',1,'arm_fir_sparse_instance_q7::stateIndex()']]],
  ['stir',['STIR',['../struct_n_v_i_c___type.html#a37de89637466e007171c6b135299bc75',1,'NVIC_Type::STIR()'],['../struct_s_c_b___type.html#ad70825dd0869b7ccd07fb2b8680fcdb6',1,'SCB_Type::STIR()']]]
];
